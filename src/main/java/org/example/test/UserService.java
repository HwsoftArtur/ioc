package org.example.test;

import lombok.RequiredArgsConstructor;
import org.example.test.container.annotations.Autowired;
import org.example.test.container.annotations.Component;

@Component
public class UserService {

    @Autowired
    private  UserDao userDao;

    public void showUsers(){
        userDao.getUsers().forEach(System.out::println);
    }
}
