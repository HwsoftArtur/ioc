package org.example.test;

import org.example.test.container.annotations.Component;

import java.util.List;

@Component
public class InMemoryUserDao implements UserDao{


    @Override
    public List<String> getUsers() {
        return List.of("Petia","Vasia");
    }
}
