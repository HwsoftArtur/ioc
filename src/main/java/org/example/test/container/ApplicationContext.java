package org.example.test.container;

import org.example.test.container.annotations.Component;
import org.example.test.container.utils.PackageScanner;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ApplicationContext {
    private Map<Class<?>,Object> beans = new HashMap<>();
    private PackageScanner packageScanner = new PackageScanner();
    private String packageName;
    private List<Class<?>> loadedClasses;

    public ApplicationContext(String packageName) {
        this.packageName = packageName;
        scanPackage();
        init();
    }

    private void init() {

        List<BeanPostProcessor> postProcessors = getComponents()
                .stream().map(this::getBean)
                .filter(o -> o instanceof BeanPostProcessor)
                .map(o->(BeanPostProcessor)o)
                .collect(Collectors.toList());

        for (BeanPostProcessor postProcessor : postProcessors) {
            for (Map.Entry<Class<?>, Object> bean : beans.entrySet()) {
                Object o = postProcessor.processBeforeInitialization(bean.getValue(),this);
                beans.put(bean.getKey(),o);
            }
        }

        for (BeanPostProcessor postProcessor : postProcessors) {
            for (Map.Entry<Class<?>, Object> bean : beans.entrySet()) {
                Object o = postProcessor.processAfterInitialization(bean.getValue(),this);
                beans.put(bean.getKey(),o);
            }
        }
    }

    private void scanPackage() {
        loadedClasses = packageScanner.scanPackage(packageName);
    }

    public <T> T getBean(Class<T> clazz){
        Object bean = getExistingBean(clazz);
        if(bean == null){
            bean = createNewInstance(clazz);
        }
        return (T)bean;
    }

    private Object createNewInstance(Class<?> clazz) {
        List<Class<?>> components = getComponents();
        List<Class<?>> compatibilityComponents = components.stream()
                .filter(clazz::isAssignableFrom)
                .collect(Collectors.toList());

        if(compatibilityComponents.size()>1){
            throw new RuntimeException("Найдено больше одного подходящего класса для "+ clazz.getName());
        }
        if(compatibilityComponents.size()==0){
            throw new RuntimeException("Не найдено подходящего класса для "+ clazz.getName());
        }

        Class<?> foundedClazz = compatibilityComponents.get(0);

        try {
            Object instance = foundedClazz.getConstructor().newInstance();
            beans.put(foundedClazz,instance);
            return instance;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException("Не найдено подходящего конструктора у "+ foundedClazz.getName());
        }
    }

    private List<Class<?>> getComponents() {
        return loadedClasses.stream()
                .filter(c -> c.isAnnotationPresent(Component.class))
                .collect(Collectors.toList());
    }

    private Object getExistingBean(Class<?> clazz) {

        for (Map.Entry<Class<?>, Object> exClass : beans.entrySet()) {
            if(clazz.isAssignableFrom(exClass.getKey())){
                return exClass.getValue();
            }
        }
        return null;
    }
}
