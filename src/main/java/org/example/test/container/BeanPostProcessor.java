package org.example.test.container;

public interface BeanPostProcessor {
    default Object processBeforeInitialization(Object bean, ApplicationContext context){return bean;}
    default Object processAfterInitialization(Object bean, ApplicationContext context){return bean;}
}
