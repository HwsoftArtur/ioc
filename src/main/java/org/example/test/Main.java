package org.example.test;

import lombok.*;
import lombok.experimental.Accessors;
import org.example.test.container.ApplicationContext;
import org.example.test.container.utils.PackageScanner;

import java.util.List;

public class Main {



    public static void main(String[] args) {
        ApplicationContext ctx = new ApplicationContext("org.example.test");

        UserService userService = ctx.getBean(UserService.class);
        userService.showUsers();

    }

}
