package org.example.test;

import java.util.List;

public interface UserDao {

    List<String> getUsers();
}
